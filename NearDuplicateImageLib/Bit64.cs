﻿using System.Text;

namespace NearDuplicateImageLib
{
    public sealed class Bit64
    {
        private const int MAX_LENGTH = sizeof(ulong) * 8;

        public Bit64(ulong value, int length = MAX_LENGTH) : this(length)
        {
            Value = value;
        }

        public Bit64(int length = MAX_LENGTH)
        {
            if (length < 1 || length > MAX_LENGTH)
            {
                throw new ArgumentOutOfRangeException(nameof(length));
            }

            Length = length;
        }

        public int Length { get; private set; }

        public ulong Value { get; private set; } = 0;

        public bool this[int index]
        {
            get
            {
                return IsSet(index);
            }

            set
            {
                SetBit(index, value);
            }
        }

        public void SetBit(int pos, bool val)
        {
            if (pos < 0 || pos >= Length)
            {
                throw new ArgumentOutOfRangeException(nameof(pos), $"Position must be < {Length}");
            }

            if (val)
            {
                SetBit(pos);
            }
            else
            {
                ClearBit(pos);
            }
        }

        public void SetBit(int pos)
        {
            if (pos < 0 || pos >= Length)
            {
                throw new ArgumentOutOfRangeException(nameof(pos), $"Position must be < {Length}");
            }

            Value |= (1ul << pos);
        }

        public void ClearBit(int pos)
        {
            if (pos < 0 || pos >= Length)
            {
                throw new ArgumentOutOfRangeException(nameof(pos), $"Position must be < {Length}");
            }

            Value &= ~(1ul << pos);
        }

        public bool IsSet(int pos)
        {
            if (pos < 0 || pos >= Length)
            {
                throw new ArgumentOutOfRangeException(nameof(pos), $"Position must be < {Length}");
            }

            return (Value & (1ul << pos)) != 0;
        }

        public bool GetBit(int pos)
        {
            if (pos < 0 || pos >= Length)
            {
                throw new ArgumentOutOfRangeException(nameof(pos), $"Position must be < {Length}");
            }

            return IsSet(pos);
        }

        public ulong Xor(Bit64 value)
        {
            return Value ^ value.Value;
        }

        public ulong HammingDistance(Bit64 value)
        {
            return CountBitsSet(Value ^ value.Value);
        }

        public ulong HammingDistance(ulong value)
        {
            return CountBitsSet(Value ^ value);
        }

        public ulong CountBitsSet()
        {
            return CountBitsSet(Value);
        }

        private ulong CountBitsSet(ulong val)
        {
            ulong num = 0;

            while (val != 0)
            {
                num += val & 1;
                val >>= 1;
            }

            return num;
        }

        override public string ToString()
        {
            StringBuilder str = new StringBuilder(64);

            for (int i = 0; i < 64; ++i)
            {
                str.Append(IsSet(i) ? "1" : "0");
            }

            return str.ToString();
        }
    }
}
