﻿namespace NearDuplicateImageLib
{
    internal sealed class Pixel<T> where T : struct
    {
        //For now assume always 4 channels even if the image is grayscale
        //The format we will use is RGBA
        private const int NUM_CHANNELS = 4;

        public Pixel()
        {
            Channels = new T[NUM_CHANNELS];
            for (int i = 0; i < NUM_CHANNELS; ++i)
                Channels[i] = default;
        }

        public T[] Channels { get; set; }
    }
}
