﻿namespace NearDuplicateImageLib
{
    public static class KernelMatrixHelper
    {
        public static KernelMatrix CalculateGaussianKernel(int length, double weight)
        {
            KernelMatrix Kernel = new KernelMatrix(length);
            double sumTotal = 0;

            int kernelRadius = length / 2;
            double distance = 0;

            double calculatedEuler = 1.0 / (2.0 * Math.PI * Math.Pow(weight, 2));

            for (int filterY = -kernelRadius; filterY <= kernelRadius; ++filterY)
            {
                for (int filterX = -kernelRadius; filterX <= kernelRadius; ++filterX)
                {
                    distance = ((filterX * filterX) + (filterY * filterY)) / (2 * (weight * weight));

                    Kernel[filterY + kernelRadius, filterX + kernelRadius] = calculatedEuler * Math.Exp(-distance);

                    sumTotal += Kernel[filterY + kernelRadius, filterX + kernelRadius];
                }
            }

            for (int y = 0; y < length; ++y)
            {
                for (int x = 0; x < length; x++)
                {
                    Kernel[y, x] = Kernel[y, x] * (1.0 / sumTotal);
                }
            }

            return Kernel;
        }
    }
}
