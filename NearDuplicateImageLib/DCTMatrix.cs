﻿namespace NearDuplicateImageLib
{
    public sealed class DCTMatrix<T> where T : struct
    {
        private readonly T[][] _dct;

        public DCTMatrix(int total_width, int total_height)
        {
            _dct = new T[total_width][];
            for (int i = 0; i < total_width; ++i)
            {
                _dct[i] = new T[total_height];
            }
        }

        public T[] this[int col]
        {
            get => _dct[col];
            set => _dct[col] = value;
        }

        public void Set(int x, int y, T val)
        {
            _dct[x][y] = val;
        }

        public T Get(int x, int y)
        {
            return _dct[x][y];
        }
    }
}
