﻿namespace NearDuplicateImageLib
{
    public class KernelMatrix
    {
        private double[,] _kernel;

        public KernelMatrix(int size)
        {
            Size = size;
            _kernel = new double[Size, Size];
        }

        public double this[int row, int col]
        {
            get => _kernel[row, col];
            set => _kernel[row, col] = value;
        }

        public void Set(int row, int col, double val)
        {
            _kernel[row, col] = val;
        }

        public double Get(int row, int col)
        {
            return _kernel[row, col];
        }

        public void SetAll(double value)
        {
            for (int y = 0; y < Size; ++y)
            {
                for (int x = 0; x < Size; ++x)
                {
                    _kernel[y, x] = value;
                }
            }
        }

        public int Size { get; } = 0;

        protected void Set(double[,] kernel)
        {
            _kernel = kernel;
        }
    }

    public sealed class Laplacian3x3 : KernelMatrix
    {
        public Laplacian3x3() : base(3)
        {
            Set(new double[,]
            {
                { -1, -1, -1, },
                { -1,  8, -1, },
                { -1, -1, -1, },
            });
        }
    }

    public sealed class Laplacian5x5 : KernelMatrix
    {
        public Laplacian5x5() : base(5)
        {
            Set(new double[,]
            {
                    { -1, -1, -1, -1, -1, },
                    { -1, -1, -1, -1, -1, },
                    { -1, -1, 24, -1, -1, },
                    { -1, -1, -1, -1, -1, },
                    { -1, -1, -1, -1, -1  }
            });
        }
    }

    public sealed class LaplacianOfGaussian : KernelMatrix
    {
        public LaplacianOfGaussian() : base(5)
        {
            Set(new double[,]
            {
                {  0,  0, -1,  0,  0 },
                {  0, -1, -2, -1,  0 },
                { -1, -2, 16, -2, -1 },
                {  0, -1, -2, -1,  0 },
                {  0,  0, -1,  0,  0 }
            });
        }
    }

    public sealed class Gaussian3x3 : KernelMatrix
    {
        public Gaussian3x3() : base(3)
        {
            Set(new double[,]
            {
                { 1, 2, 1, },
                { 2, 4, 2, },
                { 1, 2, 1, }
            });
        }
    }

    public sealed class Gaussian5x5Type1 : KernelMatrix
    {
        public Gaussian5x5Type1() : base(5)
        {
            Set(new double[,]
            {
                { 2,  4,  5,  4, 2 },
                { 4,  9, 12,  9, 4 },
                { 5, 12, 15, 12, 5 },
                { 4,  9, 12,  9, 4 },
                { 2,  4,  5,  4, 2 },
            });
        }
    }

    public sealed class Gaussian5x5Type2 : KernelMatrix
    {
        public Gaussian5x5Type2() : base(5)
        {
            Set(new double[,]
            {
                {  1,  4,  6,  4, 1 },
                {  4, 16, 24, 16, 4 },
                {  6, 24, 36, 24, 6 },
                {  4, 16, 24, 16, 4 },
                {  1,  4,  6,  4, 1 },
            });
        }
    }

    public sealed class Sobel3x3Horizontal : KernelMatrix
    {
        public Sobel3x3Horizontal() : base(3)
        {
            Set(new double[,]
            {
                { -1,  0,  1, },
                { -2,  0,  2, },
                { -1,  0,  1, },
            });
        }
    }

    public sealed class Sobel3x3Vertical : KernelMatrix
    {
        public Sobel3x3Vertical() : base(3)
        {
            Set(new double[,]
            {
                {  1,  2,  1, },
                {  0,  0,  0, },
                { -1, -2, -1, },
            });
        }
    }

    public sealed class Prewitt3x3Horizontal : KernelMatrix
    {
        public Prewitt3x3Horizontal() : base(3)
        {
            Set(new double[,]
            {
                { -1,  0,  1, },
                { -1,  0,  1, },
                { -1,  0,  1, },
            });
        }
    }

    public sealed class Prewitt3x3Vertical : KernelMatrix
    {
        public Prewitt3x3Vertical() : base(3)
        {
            Set(new double[,]
            {
                {  1,  1,  1, },
                {  0,  0,  0, },
                { -1, -1, -1, },
            });
        }
    }
}
