﻿namespace NearDuplicateImageLib
{
    public static class ImageHasher
    {
        public static Bit64 PerceptualHash(string filename)
        {
            var image = new Image2D(filename);
            return new PerceptualHash().Hash(image);
        }

        public static Bit64 DifferenceHash(string filename)
        {
            var image = new Image2D(filename);
            return new DifferenceHash().Hash(image);
        }

        public static bool MatchingHashes(Bit64 hash1, Bit64 hash2, uint threshold_match)
        {
            ulong hammingDistnace = hash1.HammingDistance(hash2);

            //If the hamming distance is <= to the threshold then we
            //consider hashes to be either identical or similar enough
            //to be considered the same
            return (hammingDistnace <= threshold_match);
        }
    }
}
