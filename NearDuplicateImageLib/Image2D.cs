﻿#nullable disable

#pragma warning disable CA1416 // Validate platform compatibility
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace NearDuplicateImageLib
{
    public sealed class Image2D
    {
        private int _width = 0;
        private int _height = 0;
        private int _row_width = 0;
        private int _image_size = 0;
        private byte[] _pixel_buffer;    //We extract the pixels using the Bitmap class which uses the format BGRA

        //For now assume we are always dealing with 4 channels - RGBA. The Bitmap class always returns the
        //channels in the format BGRA so we will have to flip them round
        private const int _num_channels = 4;

        #region public
        public Image2D(string filename)
        {
            Bitmap bitmap = new Bitmap(filename);
            Init(bitmap);
            bitmap.Dispose();
        }

        public Image2D(Bitmap bitmap)
        {
            Init(bitmap);
        }

        public Image2D(byte[] pixel_buffer, int width, int height)
        {
            _width = width;
            _height = height;
            _row_width = width * _num_channels;
            _image_size = _width * _height * _num_channels;
            _pixel_buffer = new byte[_row_width * _height];
            pixel_buffer.CopyTo(_pixel_buffer, 0);
        }

        public int Width
        {
            get => _width;
        }

        public int Height
        {
            get => _height;
        }

        public Image2D ToGrayScale()
        {
            byte[] resultBuffer = new byte[_row_width * _height];

            for (int pixel = 0; pixel < _image_size; pixel += _num_channels)
            {
                double gray_value = (_pixel_buffer[pixel] * 0.0722f) + (_pixel_buffer[pixel + 1] * 0.7152f) + (_pixel_buffer[pixel + 2] * 0.2126f);
                gray_value = Clamp(gray_value);

                resultBuffer[pixel] = (byte)gray_value;
                resultBuffer[pixel + 1] = (byte)gray_value;
                resultBuffer[pixel + 2] = (byte)gray_value;
                resultBuffer[pixel + 3] = _pixel_buffer[pixel + 3];
            }

            return new Image2D(resultBuffer, _width, _height);
        }

        public Image2D ToSepia()
        {
            byte[] resultBuffer = new byte[_row_width * _height];

            for (int pixel = 0; pixel < _image_size; pixel += _num_channels)
            {
                double sb = (_pixel_buffer[pixel] * 0.131f) + (_pixel_buffer[pixel + 1] * 0.534f) + (_pixel_buffer[pixel + 2] * 0.272f);
                double sg = (_pixel_buffer[pixel] * 0.168f) + (_pixel_buffer[pixel + 1] * 0.686f) + (_pixel_buffer[pixel + 2] * 0.349f);
                double sr = (_pixel_buffer[pixel] * 0.168f) + (_pixel_buffer[pixel + 1] * 0.769f) + (_pixel_buffer[pixel + 2] * 0.393f);

                sb = Clamp(sb);
                sg = Clamp(sg);
                sr = Clamp(sr);

                resultBuffer[pixel] = (byte)sb;
                resultBuffer[pixel + 1] = (byte)sg;
                resultBuffer[pixel + 2] = (byte)sr;
                resultBuffer[pixel + 3] = _pixel_buffer[pixel + 3];
            }

            return new Image2D(resultBuffer, _width, _height);
        }

        public Image2D ToResizedImage(int resizeWidth, int resizeHeight)
        {
            Bitmap resizeImage;

            using (Bitmap bitmap = CreateAsBitmap())
            {

                resizeImage = new Bitmap(resizeWidth, resizeHeight);

                var brush = new SolidBrush(Color.Black);

                using (var graphics = Graphics.FromImage(resizeImage))
                {
                    graphics.InterpolationMode = InterpolationMode.High;
                    graphics.CompositingQuality = CompositingQuality.HighQuality;
                    graphics.SmoothingMode = SmoothingMode.AntiAlias;

                    graphics.DrawImage(bitmap, 0, 0, resizeWidth, resizeHeight);
                }
            }

            return new Image2D(resizeImage);
        }

        public Image2D ToResizedImageWithAsepctRatio(int resizeWidth, int resizedHeight)
        {
            //https://stackoverflow.com/questions/10442269/scaling-a-system-drawing-bitmap-to-a-given-size-while-maintaining-aspect-ratio

            Bitmap resizeImage = null;

            using (Bitmap bitmap = CreateAsBitmap())
            {

                resizeImage = new Bitmap(resizeWidth, resizedHeight);

                using (var graphics = Graphics.FromImage(resizeImage))
                {
                    graphics.InterpolationMode = InterpolationMode.High;
                    graphics.CompositingQuality = CompositingQuality.HighQuality;
                    graphics.SmoothingMode = SmoothingMode.AntiAlias;

                    float scale = Math.Min(resizeWidth / bitmap.Width, resizedHeight / bitmap.Height);

                    var scaleWidth = (int)(bitmap.Width * scale);
                    var scaleHeight = (int)(bitmap.Height * scale);

                    if (scaleWidth < resizeWidth)
                        scaleWidth = resizeWidth;

                    if (scaleHeight < resizedHeight)
                        scaleHeight = resizedHeight;

                    int shiftWidth = 0;
                    int shiftHeight = 0;

                    if (scaleWidth > resizeWidth)
                        shiftWidth = (scaleWidth - resizeWidth) / 2;

                    if (scaleHeight > resizedHeight)
                        shiftHeight = (scaleHeight - resizedHeight) / 2;

                    graphics.DrawImage(bitmap, -shiftWidth, -shiftHeight, scaleWidth, scaleHeight);
                }
            }

            return new Image2D(resizeImage);
        }

        public DCTMatrix<double> ToDct()
        {
            int image_width = _row_width;
            int image_height = _height;

            double[][] rows = new double[image_height][];
            double[] row = new double[image_width];

            for (int y = 0; y < image_height; ++y)
            {
                for (int x = 0; x < image_width; ++x)
                {
                    row[x] = _pixel_buffer[y * image_width + x];
                }

                rows[y] = Dct1D(row);
            }

            DCTMatrix<double> result = new DCTMatrix<double>(image_width, image_height);

            double[] col = new double[image_height];

            for (int x = 0; x < image_width; ++x)
            {
                for (int y = 0; y < image_height; ++y)
                {
                    col[y] = rows[y][x];
                }

                result[x] = Dct1D(col);
            }

            return result;
        }

        //TODO: Update Convolve methods to use "double channels[3]" in the code instead of separate
        //variables for red, green and blue
        //TODO: Perhaps add a bool parameter to let the caller indicate whether the alpha channel should also
        //be convolved or set to be fully opaque. Currently always forcing the alpha channel to be fully opaque.
        public Image2D Convolve(KernelMatrix kernel, double factor = 1, int bias = 0)
        {
            byte[] resultBuffer = new byte[_row_width * _height];

            int num_output_channels = _num_channels - 1;

            //TODO: When we allow channels between 1 - 4 then uncomment the code below
            //if (_num_channels == 2)
            //    num_output_channels = 1;
            //else if (_num_channels == 4)
            //    num_output_channels = 3;
            //else
            //    num_output_channels = _num_channels;

            double[] channels = new double[num_output_channels];

            int kernelOffset = (kernel.Size - 1) / 2;
            int byteOffset = 0;
            int offset = 0;

            for (int offsetY = kernelOffset; offsetY < _height - kernelOffset; ++offsetY)
            {
                for (int offsetX = kernelOffset; offsetX < _width - kernelOffset; ++offsetX)
                {
                    for (int channel = 0; channel < num_output_channels; ++channel)
                    {
                        channels[channel] = 0;
                    }

                    byteOffset = offsetY * _row_width + offsetX * _num_channels;

                    for (int filterY = -kernelOffset; filterY <= kernelOffset; ++filterY)
                    {
                        for (int filterX = -kernelOffset; filterX <= kernelOffset; ++filterX)
                        {
                            offset = byteOffset + (filterX * _num_channels) + (filterY * _row_width);

                            for (int channel = 0; channel < num_output_channels; ++channel)
                            {
                                channels[channel] += (double)(_pixel_buffer[offset + channel]) * kernel[filterY + kernelOffset, filterX + kernelOffset];
                            }
                        }
                    }

                    for (int channel = 0; channel < num_output_channels; ++channel)
                    {
                        channels[channel] = factor * channels[channel] + bias;
                        channels[channel] = Clamp(channels[channel]);
                        resultBuffer[byteOffset + channel] = (byte)channels[channel];
                    }

                    //If there is an alpha channel set it to the upper value of T
                    if (_num_channels != num_output_channels)
                    {
                        //If they are not the same then num_output_channels is only ever one less
                        //than num_channels
                        resultBuffer[byteOffset + num_output_channels] = 255;      //Fully opaque
                    }
                }
            }

            return new Image2D(resultBuffer, _width, _height);
        }

        //TODO: Update Convolve methods to use "double channels[3]" in the code instead of separate
        //variables for red, green and blue
        //TODO: Perhaps add a bool parameter to let the caller indicate whether the alpha channel should also
        //be convolved or set to be fully opaque. Currently always forcing the alpha channel to be fully opaque.
        public Image2D Convolve(KernelMatrix xKernel, KernelMatrix yKernel, double factor = 1, int bias = 0)
        {
            byte[] resultBuffer = new byte[_row_width * _height];

            int num_output_channels = _num_channels - 1;

            //TODO: When we allow channels between 1 - 4 then uncomment the code below
            //if (_num_channels == 2)
            //    num_output_channels = 1;
            //else if (_num_channels == 4)
            //    num_output_channels = 3;
            //else
            //    num_output_channels = _num_channels;

            int kernelOffset = 1;
            int byteOffset = 0;
            int offset = 0;
            double[] xChannels = new double[num_output_channels];
            double[] yChannels = new double[num_output_channels];
            double[] channelsTotal = new double[num_output_channels];

            for (int offsetY = kernelOffset; offsetY < _height - kernelOffset; ++offsetY)
            {
                for (int offsetX = kernelOffset; offsetX < _width - kernelOffset; ++offsetX)
                {
                    for (int channel = 0; channel < num_output_channels; ++channel)
                    {
                        xChannels[channel] = 0;
                        yChannels[channel] = 0;
                        channelsTotal[channel] = 0;
                    }


                    byteOffset = offsetY * _row_width + offsetX * _num_channels;

                    for (int filterY = -kernelOffset; filterY <= kernelOffset; ++filterY)
                    {
                        for (int filterX = -kernelOffset; filterX <= kernelOffset; ++filterX)
                        {
                            offset = byteOffset + (filterX * _num_channels) + (filterY * _row_width);

                            for (int channel = 0; channel < num_output_channels; ++channel)
                            {
                                xChannels[channel] += (double)(_pixel_buffer[offset + channel]) * xKernel[filterY + kernelOffset, filterX + kernelOffset];
                                yChannels[channel] += (double)(_pixel_buffer[offset + channel]) * yKernel[filterY + kernelOffset, filterX + kernelOffset];
                            }
                        }
                    }

                    for (int channel = 0; channel < num_output_channels; ++channel)
                    {
                        channelsTotal[channel] = Math.Sqrt((xChannels[channel] * xChannels[channel]) + (yChannels[channel] * yChannels[channel]));
                        channelsTotal[channel] = Clamp(channelsTotal[channel]);
                        resultBuffer[byteOffset + channel] = (byte)channelsTotal[channel];
                    }

                    //If there is an alpha channel set it to the upper value of T
                    if (_num_channels != num_output_channels)
                    {
                        //If they are not the same then num_output_channels is only ever one less
                        //than num_channels
                        resultBuffer[byteOffset + num_output_channels] = 255;      //Fully opaque
                    }
                }
            }

            return new Image2D(resultBuffer, _width, _height);
        }

        public Color GetPixel(int x, int y)
        {
            int index = (y * _row_width) + (x * _num_channels);
            byte b = _pixel_buffer[index];
            byte g = _pixel_buffer[index + 1];
            byte r = _pixel_buffer[index + 2];
            byte a = _pixel_buffer[index + 3];
            return Color.FromArgb(a, r, g, b);
        }

        public void Save(string filename)
        {
            Bitmap bitmap = CreateAsBitmap();
            bitmap.Save(filename);
            bitmap.Dispose();
        }

        #endregion

        #region private

        private void Init(Bitmap bitmap)
        {
            BitmapData sourceData = bitmap.LockBits(
                new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);

            _width = sourceData.Width;
            _height = sourceData.Height;
            _row_width = sourceData.Stride;

            _pixel_buffer = new byte[sourceData.Stride * sourceData.Height];

            Marshal.Copy(sourceData.Scan0, _pixel_buffer, 0, _pixel_buffer.Length);

            bitmap.UnlockBits(sourceData);

            _image_size = _width * _height * _num_channels;
        }

        private Bitmap CreateAsBitmap()
        {
            Bitmap bitmap = new Bitmap(_width, _height);

            BitmapData sourceData =
                bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.WriteOnly, PixelFormat.Format32bppArgb);

            Marshal.Copy(_pixel_buffer, 0, sourceData.Scan0, _pixel_buffer.Length);

            bitmap.UnlockBits(sourceData);

            return bitmap;
        }


        private double[] Dct1D(double[] data)
        {
            int size = data.Length;
            double sum = 0;
            double[] result = new double[size];

            for (int i = 0; i < size; ++i)
            {
                sum = 0;
                for (int j = 0; j < size; ++j)
                {
                    sum += data[j] * Math.Cos(i * Math.PI * (2.0 * j + 1.0) / (2.0 * size));
                }

                if (i == 0)
                {
                    sum *= 1.0 / Math.Sqrt(2.0);
                }

                result[i] = sum / 2.0;
            }

            return result;
        }

        private double Clamp(double val)
        {
            if (val > 255)
                return 255;

            if (val < 0)
                return 0;

            return val;
        }

        private void SetPixel(byte[] pixels, int x, int y, int width, Pixel<byte> pixel)
        {
            for (int channel = 0; channel < _num_channels; ++channel)
            {
                pixels[y * width + x + channel] = pixel.Channels[channel];
            }
        }

        private Pixel<byte> GetPixel(byte[] pixels, int x, int y, int width)
        {
            Pixel<byte> pixel = new Pixel<byte>();

            for (int channel = 0; channel < _num_channels; ++channel)
            {
                pixel.Channels[channel] = pixels[y * width + x + channel];
            }

            return pixel;
        }
        #endregion
    }
}
#pragma warning restore CA1416 // Validate platform compatibility
