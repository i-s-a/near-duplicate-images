﻿namespace NearDuplicateImageLib
{
    public sealed class DifferenceHash : IImageHash
    {
        public Bit64 Hash(Image2D image)
        {
            const int resized_width = 9;
            const int resized_height = 8;

            var grayImage = image
                .ToResizedImage(resized_width, resized_height)
                .ToGrayScale();

            int horizontal_width = grayImage.Width - 1;

            Bit64 result = new Bit64(0UL);

            for (int y = 0; y < grayImage.Height; ++y)
            {
                byte leftPixel = grayImage.GetPixel(0, y).R;
                for (int x = 1; x < grayImage.Width; ++x)
                {
                    byte rightPixel = grayImage.GetPixel(x, y).R;
                    result.SetBit(y * horizontal_width + (x - 1), (leftPixel < rightPixel));

                    leftPixel = rightPixel;
                }
            }

            return result;
        }
    }
}
