﻿namespace NearDuplicateImageLib
{
    public interface IImageHash
    {
        Bit64 Hash(Image2D image);
    }
}
