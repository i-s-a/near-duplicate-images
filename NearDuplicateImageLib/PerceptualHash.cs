﻿namespace NearDuplicateImageLib
{
    public sealed class PerceptualHash : IImageHash
    {
        public Bit64 Hash(Image2D image)
        {
            const int resized_width = 32;
            const int resized_height = 32;

            var dct = image
                .ToResizedImage(resized_width, resized_height)
                .ToGrayScale()
                .ToDct();

            const int reduced_dct_width = 8;
            const int reduced_dct_height = 8;
            double sum = 0.0;

            for (int i = 0; i < reduced_dct_width; ++i)
            {
                for (int j = 0; j < reduced_dct_height; ++j)
                {
                    sum += dct[i][j];
                }
            }

            sum -= dct[0][0];

            double mean = sum / ((reduced_dct_width * reduced_dct_height) - 1);

            Bit64 result = new Bit64(0UL);

            for (int i = 0; i < reduced_dct_width; ++i)
            {
                for (int j = 0; j < reduced_dct_height; ++j)
                {
                    result.SetBit(j * reduced_dct_width + i, (dct[i][j] > mean));
                }
            }

            return result;
        }
    }
}
