# Near Duplicate Images #

This library provides a simple implementation to compare two images and determine if they are perceptually the same image.

## Perceptual hash algorithm ##

Crytographic hash algorithms (e.g. SHA1, SHA256, etc.) and non-cryptographic hash algorithms (e.g. xxHash, MurmurHash, etc.) produce random hash values. If the data used to generate them are identical then the hash values should be identical, but if the data is different, even by just a single bit, then the hash values should be completely different. The primary aim is to avoid collisions. Perceptual hashes are part of a family of hash algorithms that produce values that are similar when the data used to generate them are similar and produce values that are different if the data used to generate them are different. Perceptual hashes can be used with images and the values can be compared to determine if the data used to generate them are similar within a certain threshold. 

Examples of images that are perceptually similar

![Alt text](https://bitbucket.org/i-s-a/near-duplicate-images/raw/127beca76e0885074a6632586575efa92c7bec95/ImageTestFiles/Lena/lena.png "Lena")
![Alt text](https://bitbucket.org/i-s-a/near-duplicate-images/raw/bb46da324871642a01be623c227e0ccc0cfb2a84/ImageTestFiles/Lena/Lena%20-%20copyright%20bottom%20right.png "Copyright")
![Alt text](https://bitbucket.org/i-s-a/near-duplicate-images/raw/bb46da324871642a01be623c227e0ccc0cfb2a84/ImageTestFiles/Lena/lena%20-%20Pixelized%20Filter.png "Pixelated")
![Alt text](https://bitbucket.org/i-s-a/near-duplicate-images/raw/47a92b08c18ca610fed3fc5316f84a92e68961ff/ImageTestFiles/Lena/lena%20-%20Rain%20Drops%20Filter.png "Rain drops")

## Using the library ##

The library currently provides two algorithms:

+ Perceptual Hash
+ Difference Hash

Difference hash is the faster of the two algorithms but can produce more false positives than Perceptual hash. Perceptual hash is a slower algorithm but produces far fewer false positives.

    var image1 = new Image2D(imageFilename1);
	var image2 = new Image2D(imageFilename2);
	
	var perceptualHash = new PerceptualHash();
	var hash1 = perceptualHash.Hash(image1);
	var hash2 = perceptualHash.Hash(image2);
	
	const int threshold = 9;
	var matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
    
	if (matched)
	{
	    Console.WriteLine("Images matched");
	}
	else
	{
	    Console.WriteLine("Images did not match");
	}


## References ##

The library is based on the algorithm described in http://www.hackerfactor.com/blog/?/archives/432-Looks-Like-It.html

https://en.wikipedia.org/wiki/Perceptual_hashing
