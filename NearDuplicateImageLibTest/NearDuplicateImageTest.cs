using NearDuplicateImageLib;

namespace NearDuplicateImageLibTest
{
    [TestClass]
    public class NearDuplicateImageTest
    {
        private const string IMAGE_PATH = @"..\..\..\..\ImageTestFiles\";

        [TestMethod]
        public void PHash_Lena_Identical()
        {
            string imageFilename1 = IMAGE_PATH + "Lena\\lena.png";
            string imageFilename2 = IMAGE_PATH + "Lena\\lena.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.PerceptualHash(imageFilename1);

            var hash2 = ImageHasher.PerceptualHash(imageFilename2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void PHash_Lena_ExplosionFilter()
        {
            string imageFilename1 = IMAGE_PATH + "Lena\\lena.png";
            string imageFilename2 = IMAGE_PATH + "Lena\\lena - Explosion Filter.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.PerceptualHash(imageFilename1);

            var hash2 = ImageHasher.PerceptualHash(imageFilename2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void PHash_Lena_OilPaintingFilter()
        {
            string imageFilename1 = IMAGE_PATH + "Lena\\lena.png";
            string imageFilename2 = IMAGE_PATH + "Lena\\lena - Oil Paint Filter.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.PerceptualHash(imageFilename1);

            var hash2 = ImageHasher.PerceptualHash(imageFilename2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void PHash_Lena_PixelizedFilter()
        {
            string imageFilename1 = IMAGE_PATH + "Lena\\lena.png";
            string imageFilename2 = IMAGE_PATH + "Lena\\lena - Pixelized Filter.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.PerceptualHash(imageFilename1);


            var hash2 = ImageHasher.PerceptualHash(imageFilename2);


            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void PHash_Lena_RainDropsFilter()
        {
            string imageFilename1 = IMAGE_PATH + "Lena\\lena.png";
            string imageFilename2 = IMAGE_PATH + "Lena\\lena - Rain Drops Filter.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.PerceptualHash(imageFilename1);


            var hash2 = ImageHasher.PerceptualHash(imageFilename2);


            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void PHash_Lena_Rotated90()
        {
            string imageFilename1 = IMAGE_PATH + "Lena\\lena.png";
            string imageFilename2 = IMAGE_PATH + "Lena\\lena - Rotated 90.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.PerceptualHash(imageFilename1);


            var hash2 = ImageHasher.PerceptualHash(imageFilename2);


            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsFalse(matched);
        }

        [TestMethod]
        public void DHash_Lena_Identical()
        {
            string imageFilename1 = IMAGE_PATH + "Lena\\lena.png";
            string imageFilename2 = IMAGE_PATH + "Lena\\lena.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.DifferenceHash(imageFilename1);

            var hash2 = ImageHasher.DifferenceHash(imageFilename2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void DHash_Lena_ExplosionFilter()
        {
            string imageFilename1 = IMAGE_PATH + "Lena\\lena.png";
            string imageFilename2 = IMAGE_PATH + "Lena\\lena - Explosion Filter.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.DifferenceHash(imageFilename1);

            var hash2 = ImageHasher.DifferenceHash(imageFilename2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void DHash_Lena_OilPaintingFilter()
        {
            string imageFilename1 = IMAGE_PATH + "Lena\\lena.png";
            string imageFilename2 = IMAGE_PATH + "Lena\\lena - Oil Paint Filter.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.DifferenceHash(imageFilename1);

            var hash2 = ImageHasher.DifferenceHash(imageFilename2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void DHash_Lena_PixelizedFilter()
        {
            string imageFilename1 = IMAGE_PATH + "Lena\\lena.png";
            string imageFilename2 = IMAGE_PATH + "Lena\\lena - Pixelized Filter.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.DifferenceHash(imageFilename1);


            var hash2 = ImageHasher.DifferenceHash(imageFilename2);


            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void DHash_Lena_RainDropsFilter()
        {
            string imageFilename1 = IMAGE_PATH + "Lena\\lena.png";
            string imageFilename2 = IMAGE_PATH + "Lena\\lena - Rain Drops Filter.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.DifferenceHash(imageFilename1);


            var hash2 = ImageHasher.DifferenceHash(imageFilename2);


            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void DHash_Lena_Rotated90()
        {
            string imageFilename1 = IMAGE_PATH + "Lena\\lena.png";
            string imageFilename2 = IMAGE_PATH + "Lena\\lena - Rotated 90.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.DifferenceHash(imageFilename1);


            var hash2 = ImageHasher.DifferenceHash(imageFilename2);


            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsFalse(matched);
        }

        [TestMethod]
        public void PHash_Monarch_Identical()
        {
            string imageFilename1 = IMAGE_PATH + "Monarch\\monarch.png";
            string imageFilename2 = IMAGE_PATH + "Monarch\\monarch.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.PerceptualHash(imageFilename1);

            var hash2 = ImageHasher.PerceptualHash(imageFilename2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void PHash_Monarch_ExplosionFilter()
        {
            string imageFilename1 = IMAGE_PATH + "Monarch\\monarch.png";
            string imageFilename2 = IMAGE_PATH + "Monarch\\monarch - Explosion Filter.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.PerceptualHash(imageFilename1);

            var hash2 = ImageHasher.PerceptualHash(imageFilename2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void PHash_Monarch_OilPaintingFilter()
        {
            string imageFilename1 = IMAGE_PATH + "Monarch\\monarch.png";
            string imageFilename2 = IMAGE_PATH + "Monarch\\monarch - Oil Paint Filter.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.PerceptualHash(imageFilename1);

            var hash2 = ImageHasher.PerceptualHash(imageFilename2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void PHash_Monarch_PixelizedFilter()
        {
            string imageFilename1 = IMAGE_PATH + "Monarch\\monarch.png";
            string imageFilename2 = IMAGE_PATH + "Monarch\\monarch - Pixelized Filter.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.PerceptualHash(imageFilename1);


            var hash2 = ImageHasher.PerceptualHash(imageFilename2);


            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void PHash_Monarch_RainDropsFilter()
        {
            string imageFilename1 = IMAGE_PATH + "Monarch\\monarch.png";
            string imageFilename2 = IMAGE_PATH + "Monarch\\monarch - Rain Drops Filter.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.PerceptualHash(imageFilename1);


            var hash2 = ImageHasher.PerceptualHash(imageFilename2);


            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void PHash_Monarch_Rotated90()
        {
            string imageFilename1 = IMAGE_PATH + "Monarch\\monarch.png";
            string imageFilename2 = IMAGE_PATH + "Monarch\\monarch - Rotated 90.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.PerceptualHash(imageFilename1);


            var hash2 = ImageHasher.PerceptualHash(imageFilename2);


            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsFalse(matched);
        }

        [TestMethod]
        public void DHash_Monarch_Identical()
        {
            string imageFilename1 = IMAGE_PATH + "Monarch\\monarch.png";
            string imageFilename2 = IMAGE_PATH + "Monarch\\monarch.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.DifferenceHash(imageFilename1);

            var hash2 = ImageHasher.DifferenceHash(imageFilename2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void DHash_Monarch_ExplosionFilter()
        {
            string imageFilename1 = IMAGE_PATH + "Monarch\\monarch.png";
            string imageFilename2 = IMAGE_PATH + "Monarch\\monarch - Explosion Filter.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.DifferenceHash(imageFilename1);

            var hash2 = ImageHasher.DifferenceHash(imageFilename2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void DHash_Monarch_OilPaintingFilter()
        {
            string imageFilename1 = IMAGE_PATH + "Monarch\\monarch.png";
            string imageFilename2 = IMAGE_PATH + "Monarch\\monarch - Oil Paint Filter.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.DifferenceHash(imageFilename1);

            var hash2 = ImageHasher.DifferenceHash(imageFilename2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void DHash_Monarch_PixelizedFilter()
        {
            string imageFilename1 = IMAGE_PATH + "Monarch\\monarch.png";
            string imageFilename2 = IMAGE_PATH + "Monarch\\monarch - Pixelized Filter.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.DifferenceHash(imageFilename1);


            var hash2 = ImageHasher.DifferenceHash(imageFilename2);


            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void DHash_Monarch_RainDropsFilter()
        {
            string imageFilename1 = IMAGE_PATH + "Monarch\\monarch.png";
            string imageFilename2 = IMAGE_PATH + "Monarch\\monarch - Rain Drops Filter.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.DifferenceHash(imageFilename1);


            var hash2 = ImageHasher.DifferenceHash(imageFilename2);


            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void DHash_Monarch_Rotated90()
        {
            string imageFilename1 = IMAGE_PATH + "Monarch\\monarch.png";
            string imageFilename2 = IMAGE_PATH + "Monarch\\monarch - Rotated 90.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.DifferenceHash(imageFilename1);


            var hash2 = ImageHasher.DifferenceHash(imageFilename2);


            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsFalse(matched);
        }

        [TestMethod]
        public void PHash_Red_Eyed_Tree_Frog_Identical()
        {
            string imageFilename1 = IMAGE_PATH + "Red Eyed Tree Frog\\Red_eyed_tree_frog.jpg";
            string imageFilename2 = IMAGE_PATH + "Red Eyed Tree Frog\\Red_eyed_tree_frog.jpg";

            const uint threshold = 9;

            var hash1 = ImageHasher.PerceptualHash(imageFilename1);

            var hash2 = ImageHasher.PerceptualHash(imageFilename2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void PHash_Red_Eyed_Tree_Frog_ExplosionFilter()
        {
            string imageFilename1 = IMAGE_PATH + "Red Eyed Tree Frog\\Red_eyed_tree_frog.jpg";
            string imageFilename2 = IMAGE_PATH + "Red Eyed Tree Frog\\Red_eyed_tree_frog - Explosion Filter.jpg";

            const uint threshold = 9;

            var hash1 = ImageHasher.PerceptualHash(imageFilename1);

            var hash2 = ImageHasher.PerceptualHash(imageFilename2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void PHash_Red_Eyed_Tree_Frog_OilPaintingFilter()
        {
            string imageFilename1 = IMAGE_PATH + "Red Eyed Tree Frog\\Red_eyed_tree_frog.jpg";
            string imageFilename2 = IMAGE_PATH + "Red Eyed Tree Frog\\Red_eyed_tree_frog - Oil Paint Filter.jpg";

            const uint threshold = 9;

            var hash1 = ImageHasher.PerceptualHash(imageFilename1);

            var hash2 = ImageHasher.PerceptualHash(imageFilename2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void PHash_Red_Eyed_Tree_Frog_PixelizedFilter()
        {
            string imageFilename1 = IMAGE_PATH + "Red Eyed Tree Frog\\Red_eyed_tree_frog.jpg";
            string imageFilename2 = IMAGE_PATH + "Red Eyed Tree Frog\\Red_eyed_tree_frog - Pixelized Filter.jpg";

            const uint threshold = 9;

            var hash1 = ImageHasher.PerceptualHash(imageFilename1);


            var hash2 = ImageHasher.PerceptualHash(imageFilename2);


            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void PHash_Red_Eyed_Tree_Frog_RainDropsFilter()
        {
            string imageFilename1 = IMAGE_PATH + "Red Eyed Tree Frog\\Red_eyed_tree_frog.jpg";
            string imageFilename2 = IMAGE_PATH + "Red Eyed Tree Frog\\Red_eyed_tree_frog - Rain Drops Filter.jpg";

            const uint threshold = 9;

            var hash1 = ImageHasher.PerceptualHash(imageFilename1);


            var hash2 = ImageHasher.PerceptualHash(imageFilename2);


            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void PHash_Red_Eyed_Tree_Frog_Rotated90()
        {
            string imageFilename1 = IMAGE_PATH + "Red Eyed Tree Frog\\Red_eyed_tree_frog.jpg";
            string imageFilename2 = IMAGE_PATH + "Red Eyed Tree Frog\\Red_eyed_tree_frog - Rotated 90.jpg";

            const uint threshold = 9;

            var hash1 = ImageHasher.PerceptualHash(imageFilename1);


            var hash2 = ImageHasher.PerceptualHash(imageFilename2);


            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsFalse(matched);
        }

        [TestMethod]
        public void DHash_Red_Eyed_Tree_Frog_Identical()
        {
            string imageFilename1 = IMAGE_PATH + "Red Eyed Tree Frog\\Red_eyed_tree_frog.jpg";
            string imageFilename2 = IMAGE_PATH + "Red Eyed Tree Frog\\Red_eyed_tree_frog.jpg";

            const uint threshold = 9;

            var hash1 = ImageHasher.DifferenceHash(imageFilename1);

            var hash2 = ImageHasher.DifferenceHash(imageFilename2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void DHash_Red_Eyed_Tree_Frog_ExplosionFilter()
        {
            string imageFilename1 = IMAGE_PATH + "Red Eyed Tree Frog\\Red_eyed_tree_frog.jpg";
            string imageFilename2 = IMAGE_PATH + "Red Eyed Tree Frog\\Red_eyed_tree_frog - Explosion Filter.jpg";

            const uint threshold = 9;

            var hash1 = ImageHasher.DifferenceHash(imageFilename1);

            var hash2 = ImageHasher.DifferenceHash(imageFilename2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void DHash_Red_Eyed_Tree_Frog_OilPaintingFilter()
        {
            string imageFilename1 = IMAGE_PATH + "Red Eyed Tree Frog\\Red_eyed_tree_frog.jpg";
            string imageFilename2 = IMAGE_PATH + "Red Eyed Tree Frog\\Red_eyed_tree_frog - Oil Paint Filter.jpg";

            const uint threshold = 9;

            var hash1 = ImageHasher.DifferenceHash(imageFilename1);

            var hash2 = ImageHasher.DifferenceHash(imageFilename2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void DHash_Red_Eyed_Tree_Frog_PixelizedFilter()
        {
            string imageFilename1 = IMAGE_PATH + "Red Eyed Tree Frog\\Red_eyed_tree_frog.jpg";
            string imageFilename2 = IMAGE_PATH + "Red Eyed Tree Frog\\Red_eyed_tree_frog - Pixelized Filter.jpg";

            const uint threshold = 9;

            var hash1 = ImageHasher.DifferenceHash(imageFilename1);


            var hash2 = ImageHasher.DifferenceHash(imageFilename2);


            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void DHash_Red_Eyed_Tree_Frog_RainDropsFilter()
        {
            string imageFilename1 = IMAGE_PATH + "Red Eyed Tree Frog\\Red_eyed_tree_frog.jpg";
            string imageFilename2 = IMAGE_PATH + "Red Eyed Tree Frog\\Red_eyed_tree_frog - Rain Drops Filter.jpg";

            const uint threshold = 9;

            var hash1 = ImageHasher.DifferenceHash(imageFilename1);


            var hash2 = ImageHasher.DifferenceHash(imageFilename2);


            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void DHash_Red_Eyed_Tree_Frog_Rotated90()
        {
            string imageFilename1 = IMAGE_PATH + "Red Eyed Tree Frog\\Red_eyed_tree_frog.jpg";
            string imageFilename2 = IMAGE_PATH + "Red Eyed Tree Frog\\Red_eyed_tree_frog - Rotated 90.jpg";

            const uint threshold = 9;

            var hash1 = ImageHasher.DifferenceHash(imageFilename1);


            var hash2 = ImageHasher.DifferenceHash(imageFilename2);


            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsFalse(matched);
        }

        ////////////////////////////////////////////////////////////////////////////////
        //Images with transparent copyright watermark
        ////////////////////////////////////////////////////////////////////////////////

        [TestMethod]
        public void PHash_Lena_Transparent_Watermark_Top_Left()
        {
            string imageFilename1 = IMAGE_PATH + "Lena\\lena.png";
            string imageFilename2 = IMAGE_PATH + "Lena\\Lena - copyright top left transparent.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.PerceptualHash(imageFilename1);

            var hash2 = ImageHasher.PerceptualHash(imageFilename2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void PHash_Lena_Transparent_Watermark_Centre()
        {
            string imageFilename1 = IMAGE_PATH + "Lena\\lena.png";
            string imageFilename2 = IMAGE_PATH + "Lena\\Lena - copyright centre transparent.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.PerceptualHash(imageFilename1);

            var hash2 = ImageHasher.PerceptualHash(imageFilename2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void PHash_Lena_Transparent_Watermark_Bottom_Right()
        {
            string imageFilename1 = IMAGE_PATH + "Lena\\lena.png";
            string imageFilename2 = IMAGE_PATH + "Lena\\Lena - copyright bottom right transparent.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.PerceptualHash(imageFilename1);

            var hash2 = ImageHasher.PerceptualHash(imageFilename2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void PHash_Monarch_Transparent_Watermark_Top_Left()
        {
            string imageFimonarchme1 = IMAGE_PATH + "Monarch\\monarch.png";
            string imageFimonarchme2 = IMAGE_PATH + "Monarch\\Monarch - copyright top left transparent.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.PerceptualHash(imageFimonarchme1);

            var hash2 = ImageHasher.PerceptualHash(imageFimonarchme2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void PHash_Monarch_Transparent_Watermark_Centre()
        {
            string imageFimonarchme1 = IMAGE_PATH + "Monarch\\monarch.png";
            string imageFimonarchme2 = IMAGE_PATH + "Monarch\\Monarch - copyright centre transparent.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.PerceptualHash(imageFimonarchme1);

            var hash2 = ImageHasher.PerceptualHash(imageFimonarchme2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void PHash_Monarch_Transparent_Watermark_Bottom_Right()
        {
            string imageFimonarchme1 = IMAGE_PATH + "Monarch\\monarch.png";
            string imageFimonarchme2 = IMAGE_PATH + "Monarch\\Monarch - copyright bottom right transparent.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.PerceptualHash(imageFimonarchme1);

            var hash2 = ImageHasher.PerceptualHash(imageFimonarchme2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void PHash_Red_Eyed_Tree_Frog_Transparent_Watermark_Top_Left()
        {
            string imageFimonarchme1 = IMAGE_PATH + "Red Eyed Tree Frog\\Red_eyed_tree_frog.jpg";
            string imageFimonarchme2 = IMAGE_PATH + "Red Eyed Tree Frog\\Red Eyed Tree Frog - copyright top left transparent.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.PerceptualHash(imageFimonarchme1);

            var hash2 = ImageHasher.PerceptualHash(imageFimonarchme2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void PHash_Red_Eyed_Tree_Frog_Transparent_Watermark_Centre()
        {
            string imageFimonarchme1 = IMAGE_PATH + "Red Eyed Tree Frog\\Red_eyed_tree_frog.jpg";
            string imageFimonarchme2 = IMAGE_PATH + "Red Eyed Tree Frog\\Red Eyed Tree Frog - copyright centre transparent.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.PerceptualHash(imageFimonarchme1);

            var hash2 = ImageHasher.PerceptualHash(imageFimonarchme2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void PHash_Red_Eyed_Tree_Frog_Transparent_Watermark_Bottom_Right()
        {
            string imageFimonarchme1 = IMAGE_PATH + "Red Eyed Tree Frog\\Red_eyed_tree_frog.jpg";
            string imageFimonarchme2 = IMAGE_PATH + "Red Eyed Tree Frog\\Red Eyed Tree Frog - copyright bottom right transparent.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.PerceptualHash(imageFimonarchme1);

            var hash2 = ImageHasher.PerceptualHash(imageFimonarchme2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void DHash_Lena_Transparent_Watermark_Top_Left()
        {
            string imageFilename1 = IMAGE_PATH + "Lena\\lena.png";
            string imageFilename2 = IMAGE_PATH + "Lena\\Lena - copyright top left transparent.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.DifferenceHash(imageFilename1);

            var hash2 = ImageHasher.DifferenceHash(imageFilename2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void DHash_Lena_Transparent_Watermark_Centre()
        {
            string imageFilename1 = IMAGE_PATH + "Lena\\lena.png";
            string imageFilename2 = IMAGE_PATH + "Lena\\Lena - copyright centre transparent.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.DifferenceHash(imageFilename1);

            var hash2 = ImageHasher.DifferenceHash(imageFilename2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void DHash_Lena_Transparent_Watermark_Bottom_Right()
        {
            string imageFilename1 = IMAGE_PATH + "Lena\\lena.png";
            string imageFilename2 = IMAGE_PATH + "Lena\\Lena - copyright bottom right transparent.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.DifferenceHash(imageFilename1);

            var hash2 = ImageHasher.DifferenceHash(imageFilename2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void DHash_Monarch_Transparent_Watermark_Top_Left()
        {
            string imageFimonarchme1 = IMAGE_PATH + "Monarch\\monarch.png";
            string imageFimonarchme2 = IMAGE_PATH + "Monarch\\Monarch - copyright top left transparent.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.DifferenceHash(imageFimonarchme1);

            var hash2 = ImageHasher.DifferenceHash(imageFimonarchme2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void DHash_Monarch_Transparent_Watermark_Centre()
        {
            string imageFimonarchme1 = IMAGE_PATH + "Monarch\\monarch.png";
            string imageFimonarchme2 = IMAGE_PATH + "Monarch\\Monarch - copyright centre transparent.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.DifferenceHash(imageFimonarchme1);

            var hash2 = ImageHasher.DifferenceHash(imageFimonarchme2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void DHash_Monarch_Transparent_Watermark_Bottom_Right()
        {
            string imageFimonarchme1 = IMAGE_PATH + "Monarch\\monarch.png";
            string imageFimonarchme2 = IMAGE_PATH + "Monarch\\Monarch - copyright bottom right transparent.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.DifferenceHash(imageFimonarchme1);

            var hash2 = ImageHasher.DifferenceHash(imageFimonarchme2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void DHash_Red_Eyed_Tree_Frog_Transparent_Watermark_Top_Left()
        {
            string imageFimonarchme1 = IMAGE_PATH + "Red Eyed Tree Frog\\Red_eyed_tree_frog.jpg";
            string imageFimonarchme2 = IMAGE_PATH + "Red Eyed Tree Frog\\Red Eyed Tree Frog - copyright top left transparent.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.DifferenceHash(imageFimonarchme1);

            var hash2 = ImageHasher.DifferenceHash(imageFimonarchme2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void DHash_Red_Eyed_Tree_Frog_Transparent_Watermark_Centre()
        {
            string imageFimonarchme1 = IMAGE_PATH + "Red Eyed Tree Frog\\Red_eyed_tree_frog.jpg";
            string imageFimonarchme2 = IMAGE_PATH + "Red Eyed Tree Frog\\Red Eyed Tree Frog - copyright centre transparent.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.DifferenceHash(imageFimonarchme1);

            var hash2 = ImageHasher.DifferenceHash(imageFimonarchme2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void DHash_Red_Eyed_Tree_Frog_Transparent_Watermark_Bottom_Right()
        {
            string imageFimonarchme1 = IMAGE_PATH + "Red Eyed Tree Frog\\Red_eyed_tree_frog.jpg";
            string imageFimonarchme2 = IMAGE_PATH + "Red Eyed Tree Frog\\Red Eyed Tree Frog - copyright bottom right transparent.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.DifferenceHash(imageFimonarchme1);

            var hash2 = ImageHasher.DifferenceHash(imageFimonarchme2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        ////////////////////////////////////////////////////////////////////////////////
        //Images with copyright watermark
        ////////////////////////////////////////////////////////////////////////////////

        [TestMethod]
        public void PHash_Lena_Watermark_Top_Left()
        {
            string imageFilename1 = IMAGE_PATH + "Lena\\lena.png";
            string imageFilename2 = IMAGE_PATH + "Lena\\Lena - copyright top left.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.PerceptualHash(imageFilename1);

            var hash2 = ImageHasher.PerceptualHash(imageFilename2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void PHash_Lena_Watermark_Centre()
        {
            string imageFilename1 = IMAGE_PATH + "Lena\\lena.png";
            string imageFilename2 = IMAGE_PATH + "Lena\\Lena - copyright centre.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.PerceptualHash(imageFilename1);

            var hash2 = ImageHasher.PerceptualHash(imageFilename2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void PHash_Lena_Watermark_Bottom_Right()
        {
            string imageFilename1 = IMAGE_PATH + "Lena\\lena.png";
            string imageFilename2 = IMAGE_PATH + "Lena\\Lena - copyright bottom right.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.PerceptualHash(imageFilename1);

            var hash2 = ImageHasher.PerceptualHash(imageFilename2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void PHash_Monarch_Watermark_Top_Left()
        {
            string imageFimonarchme1 = IMAGE_PATH + "Monarch\\monarch.png";
            string imageFimonarchme2 = IMAGE_PATH + "Monarch\\Monarch - copyright top left.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.PerceptualHash(imageFimonarchme1);

            var hash2 = ImageHasher.PerceptualHash(imageFimonarchme2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void PHash_Monarch_Watermark_Centre()
        {
            string imageFimonarchme1 = IMAGE_PATH + "Monarch\\monarch.png";
            string imageFimonarchme2 = IMAGE_PATH + "Monarch\\Monarch - copyright centre.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.PerceptualHash(imageFimonarchme1);

            var hash2 = ImageHasher.PerceptualHash(imageFimonarchme2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void PHash_Monarch_Watermark_Bottom_Right()
        {
            string imageFimonarchme1 = IMAGE_PATH + "Monarch\\monarch.png";
            string imageFimonarchme2 = IMAGE_PATH + "Monarch\\Monarch - copyright bottom right.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.PerceptualHash(imageFimonarchme1);

            var hash2 = ImageHasher.PerceptualHash(imageFimonarchme2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void PHash_Red_Eyed_Tree_Frog_Watermark_Top_Left()
        {
            string imageFimonarchme1 = IMAGE_PATH + "Red Eyed Tree Frog\\Red_eyed_tree_frog.jpg";
            string imageFimonarchme2 = IMAGE_PATH + "Red Eyed Tree Frog\\Red Eyed Tree Frog - copyright top left.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.PerceptualHash(imageFimonarchme1);

            var hash2 = ImageHasher.PerceptualHash(imageFimonarchme2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void PHash_Red_Eyed_Tree_Frog_Watermark_Centre()
        {
            string imageFimonarchme1 = IMAGE_PATH + "Red Eyed Tree Frog\\Red_eyed_tree_frog.jpg";
            string imageFimonarchme2 = IMAGE_PATH + "Red Eyed Tree Frog\\Red Eyed Tree Frog - copyright centre.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.PerceptualHash(imageFimonarchme1);

            var hash2 = ImageHasher.PerceptualHash(imageFimonarchme2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void PHash_Red_Eyed_Tree_Frog_Watermark_Bottom_Right()
        {
            string imageFimonarchme1 = IMAGE_PATH + "Red Eyed Tree Frog\\Red_eyed_tree_frog.jpg";
            string imageFimonarchme2 = IMAGE_PATH + "Red Eyed Tree Frog\\Red Eyed Tree Frog - copyright bottom right.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.PerceptualHash(imageFimonarchme1);

            var hash2 = ImageHasher.PerceptualHash(imageFimonarchme2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void DHash_Lena_Watermark_Top_Left()
        {
            string imageFilename1 = IMAGE_PATH + "Lena\\lena.png";
            string imageFilename2 = IMAGE_PATH + "Lena\\Lena - copyright top left.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.DifferenceHash(imageFilename1);

            var hash2 = ImageHasher.DifferenceHash(imageFilename2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void DHash_Lena_Watermark_Centre()
        {
            string imageFilename1 = IMAGE_PATH + "Lena\\lena.png";
            string imageFilename2 = IMAGE_PATH + "Lena\\Lena - copyright centre.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.DifferenceHash(imageFilename1);

            var hash2 = ImageHasher.DifferenceHash(imageFilename2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void DHash_Lena_Watermark_Bottom_Right()
        {
            string imageFilename1 = IMAGE_PATH + "Lena\\lena.png";
            string imageFilename2 = IMAGE_PATH + "Lena\\Lena - copyright bottom right.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.DifferenceHash(imageFilename1);

            var hash2 = ImageHasher.DifferenceHash(imageFilename2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void DHash_Monarch_Watermark_Top_Left()
        {
            string imageFimonarchme1 = IMAGE_PATH + "Monarch\\monarch.png";
            string imageFimonarchme2 = IMAGE_PATH + "Monarch\\Monarch - copyright top left.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.DifferenceHash(imageFimonarchme1);

            var hash2 = ImageHasher.DifferenceHash(imageFimonarchme2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void DHash_Monarch_Watermark_Centre()
        {
            string imageFimonarchme1 = IMAGE_PATH + "Monarch\\monarch.png";
            string imageFimonarchme2 = IMAGE_PATH + "Monarch\\Monarch - copyright centre.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.DifferenceHash(imageFimonarchme1);

            var hash2 = ImageHasher.DifferenceHash(imageFimonarchme2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void DHash_Monarch_Watermark_Bottom_Right()
        {
            string imageFimonarchme1 = IMAGE_PATH + "Monarch\\monarch.png";
            string imageFimonarchme2 = IMAGE_PATH + "Monarch\\Monarch - copyright bottom right.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.DifferenceHash(imageFimonarchme1);

            var hash2 = ImageHasher.DifferenceHash(imageFimonarchme2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void DHash_Red_Eyed_Tree_Frog_Watermark_Top_Left()
        {
            string imageFimonarchme1 = IMAGE_PATH + "Red Eyed Tree Frog\\Red_eyed_tree_frog.jpg";
            string imageFimonarchme2 = IMAGE_PATH + "Red Eyed Tree Frog\\Red Eyed Tree Frog - copyright top left.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.DifferenceHash(imageFimonarchme1);

            var hash2 = ImageHasher.DifferenceHash(imageFimonarchme2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void DHash_Red_Eyed_Tree_Frog_Watermark_Centre()
        {
            string imageFimonarchme1 = IMAGE_PATH + "Red Eyed Tree Frog\\Red_eyed_tree_frog.jpg";
            string imageFimonarchme2 = IMAGE_PATH + "Red Eyed Tree Frog\\Red Eyed Tree Frog - copyright centre.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.DifferenceHash(imageFimonarchme1);

            var hash2 = ImageHasher.DifferenceHash(imageFimonarchme2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }

        [TestMethod]
        public void DHash_Red_Eyed_Tree_Frog_Watermark_Bottom_Right()
        {
            string imageFimonarchme1 = IMAGE_PATH + "Red Eyed Tree Frog\\Red_eyed_tree_frog.jpg";
            string imageFimonarchme2 = IMAGE_PATH + "Red Eyed Tree Frog\\Red Eyed Tree Frog - copyright bottom right.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.DifferenceHash(imageFimonarchme1);

            var hash2 = ImageHasher.DifferenceHash(imageFimonarchme2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsTrue(matched);
        }


        ////////////////////////////////////////////////////////////////////////////////
        //Images defaced
        ////////////////////////////////////////////////////////////////////////////////

        [TestMethod]
        public void PHash_Lena_Defaced()
        {
            string imageFilename1 = IMAGE_PATH + "Lena\\lena.png";
            string imageFilename2 = IMAGE_PATH + "Lena\\lena - defaced.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.PerceptualHash(imageFilename1);

            var hash2 = ImageHasher.PerceptualHash(imageFilename2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsFalse(matched);
        }

        [TestMethod]
        public void PHash_Monarch_Defaced()
        {
            string imageFilename1 = IMAGE_PATH + "Monarch\\monarch.png";
            string imageFilename2 = IMAGE_PATH + "Monarch\\monarch - defaced.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.PerceptualHash(imageFilename1);

            var hash2 = ImageHasher.PerceptualHash(imageFilename2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsFalse(matched);
        }

        [TestMethod]
        public void PHash_Red_Eyed_Tree_Frog_Defaced()
        {
            string imageFilename1 = IMAGE_PATH + "Red Eyed Tree Frog\\Red_eyed_tree_frog.jpg";
            string imageFilename2 = IMAGE_PATH + "Red Eyed Tree Frog\\Red_eyed_tree_frog - defaced.jpg";

            const uint threshold = 9;

            var hash1 = ImageHasher.PerceptualHash(imageFilename1);

            var hash2 = ImageHasher.PerceptualHash(imageFilename2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsFalse(matched);
        }

        [TestMethod]
        public void DHash_Lena_Defaced()
        {
            string imageFilename1 = IMAGE_PATH + "Lena\\lena.png";
            string imageFilename2 = IMAGE_PATH + "Lena\\lena - defaced.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.DifferenceHash(imageFilename1);

            var hash2 = ImageHasher.DifferenceHash(imageFilename2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsFalse(matched);
        }

        [TestMethod]
        public void DHash_Monarch_Defaced()
        {
            string imageFilename1 = IMAGE_PATH + "Monarch\\monarch.png";
            string imageFilename2 = IMAGE_PATH + "Monarch\\monarch - defaced.png";

            const uint threshold = 9;

            var hash1 = ImageHasher.DifferenceHash(imageFilename1);

            var hash2 = ImageHasher.DifferenceHash(imageFilename2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsFalse(matched);
        }

        [TestMethod]
        public void DHash_Red_Eyed_Tree_Frog_Defaced()
        {
            string imageFilename1 = IMAGE_PATH + "Red Eyed Tree Frog\\Red_eyed_tree_frog.jpg";
            string imageFilename2 = IMAGE_PATH + "Red Eyed Tree Frog\\Red_eyed_tree_frog - defaced.jpg";

            const uint threshold = 9;

            var hash1 = ImageHasher.DifferenceHash(imageFilename1);

            var hash2 = ImageHasher.DifferenceHash(imageFilename2);

            bool matched = ImageHasher.MatchingHashes(hash1, hash2, threshold);
            Assert.IsFalse(matched);
        }

        [TestMethod]
        public void PHash_FileDoesNotExist()
        {
            string imageFilename1 = IMAGE_PATH + "Random\\Random.jpg";

            Assert.ThrowsException<ArgumentException>(() =>
            {
                ImageHasher.PerceptualHash(imageFilename1);
            });
        }

        [TestMethod]
        public void DHash_FileDoesNotExist()
        {
            string imageFilename1 = IMAGE_PATH + "Random\\Random.jpg";

            Assert.ThrowsException<ArgumentException>(() =>
            {
                ImageHasher.DifferenceHash(imageFilename1);
            });
        }
    }
}